package _08final.mvc.controller;

import _08final.mvc.model.*;
import _08final.mvc.view.GamePanel;
import _08final.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

/**
 *    ____       _______         ____ _______ _____   ____  _____  _    _ ______
 *  / ____|   /\|__   __|/\    / ____|__   __|  __ \ / __ \|  __ \| |  | |  ____|
 * | |       /  \  | |  /  \  | (___    | |  | |__) | |  | | |__) | |__| | |__
 * | |      / /\ \ | | / /\ \  \___ \   | |  |  _  /| |  | |  ___/|  __  |  __|
 * | |____ / ____ \| |/ ____ \ ____) |  | |  | | \ \| |__| | |    | |  | | |____
 *  \_____/_/    \_\_/_/    \_\_____/   |_|  |_|  \_\\____/|_|    |_|  |_|______|
 *
 * @author Patrick Lau
 * @version 1.0
 */
public class Game implements Runnable, KeyListener {

    // ===============================================
    // FIELDS
    // ===============================================

    // sets game size and dimensions
    public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
    private GamePanel gmpPanel;
    public static Random R = new Random();
    // sets milliseconds between screen updates (animation)
    public final static int ANI_DELAY = 45;
    private Thread thrAnim;
    private static int nLevel = 1;
    private static int nTick = 0;
    private static int nVacuumCount = 0;
    private static int nVacuumsKilled = 0;
    public static int nSpawnCorner = 0;
    private static int nVacuumsSpawnCountdown;

    private static boolean bMissileLoaded = false;

    private boolean bMuted = false;


    private final int PAUSE = 80,   // p key - pause the game
            QUIT = 81,              // q key - quit
            LEFT = 37,              // left arrow - move left
            RIGHT = 39,             // right arrow - move right
            UP = 38,                // up arrow - move up
            DOWN = 40,              // down arrow - move down
            START = 83,             // s key - start the game
            FIRE = 32,              // space key - attack
            MUTE = 77,              // m key - mute music

    // below commented out but could be used in the future
    // HYPER = 68, 			// d key
    // SHIELD = 65, 		// a key arrow
    // NUM_ENTER = 10, 		// hyp
    SPECIAL = 70;           // fire special weapon;  F key

    private Clip clpThrust;
    private Clip clpMusicBackground;

    private static final int SPAWN_NEW_SHIP_FLOATER = 1200;

    // ===============================================
    // CONSTRUCTOR
    // ===============================================

    // sets up the game panel and tells the CommandCenter that this is the first game being played
    public Game() {

        gmpPanel = new GamePanel(DIM);
        gmpPanel.addKeyListener(this);
        clpThrust = Sound.clipForLoopFactory("walking.wav");
        clpMusicBackground = Sound.clipForLoopFactory("soundtrack.wav");

    }

    // ===============================================
    // METHODS
    // ===============================================

    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
            public void run() {
                try {
                    Game game = new Game(); // construct itself
                    game.fireUpAnimThread();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fireUpAnimThread() { // called initially
        if (thrAnim == null) {
            thrAnim = new Thread(this); // pass the thread a runnable object (this)
            thrAnim.start();
        }
    }

    // implements runnable - must have run method
    public void run() {

        // lower this thread's priority; let the "main" aka 'Event Dispatch'
        // thread do what it needs to do first
        thrAnim.setPriority(Thread.MIN_PRIORITY);

        // and get the current time
        long lStartTime = System.currentTimeMillis();

        // this thread animates the scene
        while (Thread.currentThread() == thrAnim) {
            tick();
            spawnNewShipFloater();
            spawnNewMouse();
            gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must
            // surround the sleep() in a try/catch block
            // this simply controls delay time between
            // the frames of the animation

            //this might be a good place to check for collisions
            checkCollisions();
            checkCollisionsFoes();
            //this might be a god place to check if the level is clear (no more foes)
            //if the level is clear then spawn some big litterboxes -- the number of litterboxes
            //should increase with the level.
            checkNewLevel();

            try {
                // The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update)
                // between frames takes longer than ANI_DELAY, then the difference between lStartTime -
                // System.currentTimeMillis() will be negative, then zero will be the sleep time
                lStartTime += ANI_DELAY;
                Thread.sleep(Math.max(0,
                        lStartTime - System.currentTimeMillis()));
            } catch (InterruptedException e) {
                // just skip this frame -- no big deal
                continue;
            }
        } // end while
    } // end run

    private void checkCollisions() {

        Point pntFriendCenter, pntFoeCenter;
        int nFriendRadiux, nFoeRadiux;

        // check for collision between falcon and foes
        for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
            for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

                pntFriendCenter = movFriend.getCenter();
                pntFoeCenter = movFoe.getCenter();
                nFriendRadiux = movFriend.getRadius();
                nFoeRadiux = movFoe.getRadius();

                // detect collision
                if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) { // stupid algorithm complexity
                    if (movFriend instanceof Falcon) {
                        // behavior with a mouse
                        if (movFoe instanceof MouseFloater) { // cat will kill the mouse
                            killFoe(movFoe);
                            debrisCloud(movFoe, 10, 50);
                            bMissileLoaded = true; // cat can now throw up (shoot cruise missiles)
                            Sound.playSound("shieldup.wav");
                        }
                        // only vacuums kill the cat
                        if (!CommandCenter.getInstance().getFalcon().getProtected() && movFoe instanceof Vacuum) {
                            CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                            CommandCenter.getInstance().spawnFalcon(false);
                            // death creates debris cloud
                            debrisCloud(movFoe, 10, 50);
                        }
                        // when cat is in a litterbox, make a pretty debris cloud
                        if (movFoe instanceof Litterbox) {
                            // creates debris cloud
                            Point debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFriend.getCenter().x - Game.R.nextInt(60), movFriend.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                        }
                        // when the cat is in invincible mode, then it kills vacuums on contact
                        if (CommandCenter.getInstance().getFalcon().getProtected() && movFoe instanceof Vacuum) {
                            killFoe(movFoe);
                            // death creates debris cloud
                            debrisCloud(movFoe, 10, 50);
                        }
                    }
                    // cruise missiles will kill the vacuum
                    if (movFriend instanceof Cruise && movFoe instanceof Vacuum) {
                        killFoe(movFoe);
                        // create some debris
                        Point debrisPoint = new Point(movFoe.getCenter().x - Game.R.nextInt(60), movFoe.getCenter().y + Game.R.nextInt(20));
                        CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                CollisionOp.Operation.ADD);
                        debrisPoint = new Point(movFoe.getCenter().x - Game.R.nextInt(60), movFoe.getCenter().y + Game.R.nextInt(20));
                        CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                CollisionOp.Operation.ADD);
                        debrisPoint = new Point(movFoe.getCenter().x - Game.R.nextInt(60), movFoe.getCenter().y + Game.R.nextInt(20));
                        CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                CollisionOp.Operation.ADD);
                        debrisPoint = new Point(movFoe.getCenter().x - Game.R.nextInt(60), movFoe.getCenter().y + Game.R.nextInt(20));
                        CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                CollisionOp.Operation.ADD);
                        // remove the foe
                        CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                    }
                    // hairballs will stop the vacuum's movement
                    if (movFriend instanceof Bullet && movFoe instanceof Vacuum) {
                        ((Sprite) movFoe).setDeltaX(0); // hairballs stop the vacuum from moving
                        // Sound.playSound("whitenoise.wav");
                    }
                }
            }
        }
        //check for collisions between falcon and floaters
        if (CommandCenter.getInstance().getFalcon() != null) {
            Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();
            int nFalRadiux = CommandCenter.getInstance().getFalcon().getRadius();
            Point pntFloaterCenter;
            int nFloaterRadiux;

            for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
                pntFloaterCenter = movFloater.getCenter();
                nFloaterRadiux = movFloater.getRadius();

                // detect collision and apply collision effects
                if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {
                    CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
                    CommandCenter.getInstance().getFalcon().setExpire(50); // activates invincibility mode
                    Sound.playSound("pacman_eatghost.wav");
                }
            }
        }
        //we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists
        // while iterating them
        while (!CommandCenter.getInstance().getOpsList().isEmpty()) {
            CollisionOp cop = CommandCenter.getInstance().getOpsList().dequeue();
            Movable mov = cop.getMovable();
            CollisionOp.Operation operation = cop.getOperation();

            switch (mov.getTeam()) {
                case FOE:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFoes().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFoes().remove(mov);
                    }
                    break;
                case FRIEND:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFriends().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFriends().remove(mov);
                    }
                    break;
                case FLOATER:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFloaters().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFloaters().remove(mov);
                    }
                    break;
                case DEBRIS:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovDebris().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovDebris().remove(mov);
                    }
                    break;
            }
        }
        // a request to the JVM is made every frame to garbage collect
        System.gc();
    }

    private void checkCollisionsFoes() {

        Point pntFoeOuterCenter; // the foe in the outer for loop
        Point pntFoeInnerCenter; // the foe in the inner foe loop

        int nFoeOuterRadiux;
        int nFoeInnerRadiux;

        // check for collision between foes and foes
        for (Movable movFoeOuter : CommandCenter.getInstance().getMovFoes()) {
            for (Movable movFoeInner : CommandCenter.getInstance().getMovFoes()) { // stupid algorithm complexity
                if (movFoeOuter instanceof Litterbox && movFoeInner instanceof Litterbox) { // litterboxes do not interact
                    break;
                }
                if (movFoeOuter != movFoeInner) {
                    pntFoeOuterCenter = movFoeOuter.getCenter();
                    pntFoeInnerCenter = movFoeInner.getCenter();
                    nFoeOuterRadiux = movFoeOuter.getRadius();
                    nFoeInnerRadiux = movFoeInner.getRadius();
                    // detect collision
                    if (pntFoeOuterCenter.distance(pntFoeInnerCenter) < (nFoeOuterRadiux + nFoeInnerRadiux)) {
                        // behavior when two vacuums collide
                        if (movFoeOuter instanceof Vacuum && movFoeInner instanceof Vacuum) {
                            // the vacuum will face the other direction
                            //((Sprite) (movFoeOuter)).setOrientation((((Sprite) (movFoeOuter)).getOrientation() + 180) % 360);
                            // the vaccuum will move a little faster
                            ((Sprite) (movFoeOuter)).setDeltaX((((Sprite) (movFoeOuter)).getDeltaX() * 1.10));
                            // increase the collision count for the vacuum
                            ((Vacuum) (movFoeOuter)).increaseCollisionCount();
                            ((Vacuum) (movFoeInner)).setCollisionCount(0);
                            // if going up, move it up more to help reduce additional collision
                            if (((Vacuum) (movFoeOuter)).getVerticalDirection()) { // if going up
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (pntFoeOuterCenter.getX()),
                                        (int) (pntFoeOuterCenter.getY() + 25)));
                                //((Sprite) (movFoeInner)).setCenter(new Point((int) (pntFoeInnerCenter.getX()),
                                // (int) (pntFoeInnerCenter.getY() - 15)));
                            }
                            // if going down, move it down more to help reduce additional collision
                            if (!(((Vacuum) (movFoeOuter)).getVerticalDirection())) { // if going down
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (pntFoeOuterCenter.getX()),
                                        (int) (pntFoeOuterCenter.getY() - 25)));
                                //((Sprite) (movFoeInner)).setCenter(new Point((int) (pntFoeInnerCenter.getX()),
                                // (int) (pntFoeInnerCenter.getY() + 15)));
                            }
                            // make it bounce backward if going right
                            if (((Vacuum) (movFoeOuter)).getDeltaX() > 0) {
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (pntFoeOuterCenter.getX() - 50),
                                        (int) (pntFoeOuterCenter.getY() + 10)));
                                //((Sprite) (movFoeInner)).setCenter(new Point((int) (pntFoeInnerCenter.getX() + 15),
                                // (int) (pntFoeInnerCenter.getY()-10)));
                            }
                            // make it bounce backward if going left
                            if (((Vacuum) (movFoeOuter)).getDeltaX() < 0) {
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (pntFoeOuterCenter.getX() + 50),
                                        (int) (pntFoeOuterCenter.getY() - 10)));
                                //((Sprite) (movFoeInner)).setCenter(new Point((int) (pntFoeInnerCenter.getX() - 15),
                                // (int) (pntFoeInnerCenter.getY()+10)));
                            }
                            // reverse the direction of the vacuum
                            ((Sprite) (movFoeOuter)).setDeltaX((((Sprite) (movFoeOuter)).getDeltaX() * -1));
                            ((Vacuum) (movFoeOuter)).setVerticalDirection();

                            // vacuums explode when they hit each other too many times
                            if (((Vacuum) (movFoeOuter)).getCollisionCount() > 100) {
                                killFoe(movFoeOuter);
                                scatterShards(movFoeOuter);
                                CommandCenter.getInstance().getOpsList().enqueue(movFoeOuter,
                                        CollisionOp.Operation.REMOVE);
                            }
                        }
                        // behavior when vacuum collides with a mouse
                        if (movFoeOuter instanceof Vacuum && movFoeInner instanceof MouseFloater) {
                            // create some debris on the mouse
                            Point debrisPoint = new Point(movFoeInner.getCenter().x - Game.R.nextInt(60), movFoeInner.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFoeInner.getCenter().x - Game.R.nextInt(60), movFoeInner.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFoeInner.getCenter().x - Game.R.nextInt(60), movFoeInner.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFoeInner.getCenter().x - Game.R.nextInt(60), movFoeInner.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            // create some debris on the vacuum
                            debrisPoint = new Point(movFoeOuter.getCenter().x - Game.R.nextInt(60), movFoeOuter.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            debrisPoint = new Point(movFoeOuter.getCenter().x - Game.R.nextInt(60), movFoeOuter.getCenter().y + Game.R.nextInt(20));
                            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                                    CollisionOp.Operation.ADD);
                            // remove the mouse
                            CommandCenter.getInstance().getOpsList().enqueue(movFoeInner,
                                    CollisionOp.Operation.REMOVE);
                            // and spawn a replacement mouse
                            spawnNewMouse();

                        }
                        // behavior when vacuum collides with a litterbox
                        if (movFoeOuter instanceof Vacuum && movFoeInner instanceof Litterbox) {
                            // increase the collision count for the vacuum
                            ((Vacuum) (movFoeOuter)).increaseCollisionCount();
                            // if going up, move the sofa up more to help reduce additional collision
                            if (((Vacuum) (movFoeOuter)).getVerticalDirection()) { // if going up
                                ((Sprite) (movFoeInner)).setCenter(new Point((int) (movFoeInner.getCenter().getX()),
                                        (int) (movFoeInner.getCenter().getY() - 10)));
                            }
                            // if going down, move the sofa down more to help reduce additional collision
                            if (!(((Vacuum) (movFoeOuter)).getVerticalDirection())) { // if going down
                                ((Sprite) (movFoeInner)).setCenter(new Point((int) (movFoeInner.getCenter().getX()),
                                        (int) (movFoeInner.getCenter().getY() + 10)));
                            }
                            // if moving right, then push the sofa a bit to the right
                            if (!(((Vacuum) (movFoeOuter)).getDeltaX() > 0)) { // if going right
                                ((Sprite) (movFoeInner)).setCenter(new Point((int) (movFoeInner.getCenter().getX() - 10),
                                        (int) (movFoeInner.getCenter().getY())));
                                // and the vacuum bounces back
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (movFoeOuter.getCenter().getX() + 50),
                                        (int) (movFoeOuter.getCenter().getY())));
                            }
                            // if moving left, then push the sofa a bit to the left
                            if (!(((Vacuum) (movFoeOuter)).getDeltaX() <= 0)) { // if going left
                                ((Sprite) (movFoeInner)).setCenter(new Point((int) (movFoeInner.getCenter().getX() + 10),
                                        (int) (movFoeInner.getCenter().getY())));
                                // and the vacuum bounces back
                                ((Sprite) (movFoeOuter)).setCenter(new Point((int) (movFoeOuter.getCenter().getX() - 50),
                                        (int) (movFoeOuter.getCenter().getY())));
                            }
                            // the vacuum will reverse direction
                            ((Sprite) (movFoeOuter)).setDeltaX((((Sprite) (movFoeOuter)).getDeltaX() * -1));
                            // the vacuum will move a little faster
                            ((Sprite) (movFoeOuter)).setDeltaX((((Sprite) (movFoeOuter)).getDeltaX() * 1.10));
                        }
                    }
                }
            }
        }
    }

    private void killFoe(Movable movFoe) {

        if (movFoe instanceof Vacuum) {
            Sound.playSound("kapow.wav");
            nVacuumsKilled++;
            scatterShards(movFoe); // added this to scatter debris when vacuums are destroyed
        }
        // remove the original Foe
        CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
        if (movFoe instanceof Scorable) {
            CommandCenter.getInstance().setScore(
                    CommandCenter.getInstance().getScore() + ((Scorable) movFoe).getScore());
        }
        System.out.println("NOM NOM NOM! THAT WAS YUMMY!");

    }

    //some methods for timing events in the game,
    //such as the appearance of UFOs, floaters (power-ups), etc.
    public void tick() {
        if (nTick == Integer.MAX_VALUE) {
            nTick = 0;
        } else {
            nTick++;
        }
    }

    public int getTick() {
        return nTick;
    }

    private void spawnNewShipFloater() {
        // make the appearance of power-up dependent upon ticks and levels
        // the higher the level the more frequent the appearance
        if (nTick % (200 - nLevel * 7) == 0) {
            //CommandCenter.getInstance().getMovFloaters().enqueue(new NewShipFloater());
            CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
        }
    }

    public static void spawnNewMouse() {
        //make the appearance of power-up dependent upon ticks and levels
        //the higher the level the more frequent the appearance
        if (nTick % (200 - nLevel * 7) == 0) {
            //CommandCenter.getInstance().getMovFloaters().enqueue(new NewShipFloater());
            CommandCenter.getInstance().getOpsList().enqueue(new MouseFloater(), CollisionOp.Operation.ADD);
        }
    }

    // called to start the game when user presses 's'
    private void startGame() {
        CommandCenter.getInstance().clearAll();
        CommandCenter.getInstance().initGame();
        CommandCenter.getInstance().setLevel(0);
        CommandCenter.getInstance().setPlaying(true);
        CommandCenter.getInstance().setPaused(false);
        // spawn two mice at the beginning
        CommandCenter.getInstance().getOpsList().enqueue(new MouseFloater(), CollisionOp.Operation.ADD);
        CommandCenter.getInstance().getOpsList().enqueue(new MouseFloater(), CollisionOp.Operation.ADD);
        // comment this line to mute music on start
        if (!bMuted) {
            clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
        }
    }

    // this method spawns new litterboxes
    private void spawnLitterboxes(int nNum) {
        for (int nC = 0; nC < nNum; nC++) {
            CommandCenter.getInstance().getOpsList().enqueue(new Litterbox(), CollisionOp.Operation.ADD);
        }
    }

    // this method spawns a vacuum cleaner
    public static void spawnVacuum(int moveSpeed) {
        CommandCenter.getInstance().getOpsList().enqueue(new Vacuum(moveSpeed + (nVacuumCount / 3), spawnVacuumCornerPoint()), CollisionOp.Operation.ADD);
    }

    // this method spawns vacuums according to the level difficulty
    public static void spawnVacuumCountdown() {
        if (nVacuumsSpawnCountdown > 0) {
            spawnVacuum((int) (CommandCenter.getInstance().getLevel() * 2.0 + 10));
            nVacuumsSpawnCountdown--;
        }
    }

    public static int spawnVacuumCornerPoint() {
        nSpawnCorner = nVacuumCount % 4;
        nVacuumCount++;
        return nSpawnCorner;
    }

    private boolean isLevelClear() {
        // if there are no more vacuums on the screen
        boolean bVacuumsFree = true;
        for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
            if (movFoe instanceof Vacuum) {
                bVacuumsFree = false;
                break;
            }
        }
        return bVacuumsFree;
    }


    private void checkNewLevel() {

        if (isLevelClear()) {
            if (CommandCenter.getInstance().getFalcon() != null) {
                CommandCenter.getInstance().getFalcon().setProtected(true);
                CommandCenter.getInstance().getFalcon().setExpire(1); // reset invincibility
            }
            // increment the level upon clear
            CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);

            // reset litterboxes
            for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
                if (movFoe instanceof Litterbox) {
                    CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
                }
            }
            // reset throw up special power
            bMissileLoaded = false;
            for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
                if (movFriend instanceof Cruise) { // remove any stray cruises
                    CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                }
                if (movFriend instanceof Falcon) {
                    ((Falcon)(movFriend)).setColor(Color.WHITE);
                }
            }
            nVacuumsSpawnCountdown = Math.min(CommandCenter.getInstance().getLevel(), 4);
            spawnVacuumCountdown();
            // scales with level
            spawnLitterboxes(Game.R.nextInt(3) + CommandCenter.getInstance().getLevel() / 2);
        }
    }

    // varargs for stopping looping-music-clips
    private static void stopLoopingSounds(Clip... clpClips) {
        for (Clip clp : clpClips) {
            clp.stop();
        }
    }
    // ===============================================
    // KEYLISTENER METHODS
    // ===============================================

    @Override
    public void keyPressed(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();
        // System.out.println(nKey);

        if (nKey == START && !CommandCenter.getInstance().isPlaying()) {
            startGame();
        }

        if (fal != null) {

            switch (nKey) {
                case PAUSE:
                    CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
                    if (CommandCenter.getInstance().isPaused()) {
                        stopLoopingSounds(clpMusicBackground, clpThrust);
                    } else {
                        clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    break;
                case QUIT:
                    System.exit(0);
                    break;

                // modified direction keys by removing thrust and rotate
                // also modified code so that movement keys do not trigger events during pause
                // direction keys now trigger zero velocity planar movement
                case UP:
                    if (!CommandCenter.getInstance().isPaused()) {
                        CommandCenter.getInstance().getFalcon().setLastOrientation(Sprite.UP);
                        fal.move(Sprite.UP);
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    break;
                case DOWN:
                    if (!CommandCenter.getInstance().isPaused()) {
                        CommandCenter.getInstance().getFalcon().setLastOrientation(Sprite.DOWN);
                        fal.move(Sprite.DOWN);
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    break;
                case LEFT:
                    if (!CommandCenter.getInstance().isPaused()) {
                        fal.move(Sprite.LEFT);
                        CommandCenter.getInstance().getFalcon().setLastOrientation(Sprite.LEFT);
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    break;
                case RIGHT:
                    if (!CommandCenter.getInstance().isPaused()) {
                        CommandCenter.getInstance().getFalcon().setLastOrientation(Sprite.RIGHT);
                        fal.move(Sprite.RIGHT);
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    break;

                // possible future use
                // case KILL:
                // case SHIELD:
                // case NUM_ENTER:

                default:
                    break;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();

        if (fal != null) {
            switch (nKey) {
                case FIRE:
                    CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
                    Sound.playSound("laser.wav");
                    break;

                // special is a special weapon, current it just fires the cruise missile.
                case SPECIAL:
                    if (bMissileLoaded) {
                        CommandCenter.getInstance().getOpsList().enqueue(new Cruise(fal), CollisionOp.Operation.ADD);
                        Sound.playSound("laser.wav");
                    }
                    break;

                case UP:
                    fal.stopMove(Sprite.UP);
                    clpThrust.stop();
                    break;
                case DOWN:
                    fal.stopMove(Sprite.DOWN);
                    clpThrust.stop();
                    break;
                case LEFT:
                    fal.stopMove(Sprite.LEFT);
                    clpThrust.stop();
                    break;
                case RIGHT:
                    fal.stopMove(Sprite.RIGHT);
                    clpThrust.stop();
                    break;
                case MUTE:
                    if (!bMuted) {
                        stopLoopingSounds(clpMusicBackground);
                        bMuted = !bMuted;
                    } else {
                        clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
                        bMuted = !bMuted;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public static int getVacuumKills() {
        return nVacuumsKilled;
    }

    @Override
    // just need it b/c of KeyListener implementation
    public void keyTyped(KeyEvent e) {
    }

    private void scatterShards(Movable movable) {
        if (movable instanceof Sprite) {
            for (int nC = 0; nC < 360; nC = nC + 20) {
                CommandCenter.getInstance().getOpsList().enqueue(
                        new Shard((Sprite) movable, nC), CollisionOp.Operation.ADD);
            }
        } else {
            return;
        }
    }

    public static void debrisCloud(Movable movable, int nSize, int nSpread) {
        Point debrisPoint;
        if (movable instanceof Sprite) {
            for (int nC = 0; nC < 360; nC = nC + 40) {
                debrisPoint = new Point(movable.getCenter().x + Game.R.nextInt(nSpread), movable.getCenter().y + Game.R.nextInt(nSpread));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(10, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x - Game.R.nextInt(nSpread), movable.getCenter().y - Game.R.nextInt(nSpread));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(10, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x + Game.R.nextInt(nSpread), movable.getCenter().y - Game.R.nextInt(nSpread));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(10, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x - Game.R.nextInt(nSpread), movable.getCenter().y + Game.R.nextInt(nSpread));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(nSize, debrisPoint),
                        CollisionOp.Operation.ADD);
            }
        } else {
            return;
        }
    }

    public static boolean getMissileStatus(){
        return bMissileLoaded;
    }
}


