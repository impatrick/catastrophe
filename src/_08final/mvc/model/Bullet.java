package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class Bullet extends Sprite {

    private final double FIRE_POWER = 35.0;

    public Bullet(Falcon fal) {

        super();
        setTeam(Team.FRIEND);

        // defined the points on a cartesian grid
        ArrayList<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(1, -3));
        pntCs.add(new Point(-67, 74));
        pntCs.add(new Point(-4, 5));
        pntCs.add(new Point(-7, 127));
        pntCs.add(new Point(5, 12));
        pntCs.add(new Point(69, 116));
        pntCs.add(new Point(30, 2));
        pntCs.add(new Point(127, 76));
        pntCs.add(new Point(49, -12));
        pntCs.add(new Point(137, -61));
        pntCs.add(new Point(54, -37));
        pntCs.add(new Point(78, -114));
        pntCs.add(new Point(23, -34));
        pntCs.add(new Point(-5, -122));
        pntCs.add(new Point(7, -21));
        pntCs.add(new Point(-85, -110));
        pntCs.add(new Point(-31, -31));
        pntCs.add(new Point(-109, -45));
        pntCs.add(new Point(-28, -1));
        pntCs.add(new Point(-126, 52));
        pntCs.add(new Point(-10, 12));
        pntCs.add(new Point(-34, 111));
        pntCs.add(new Point(-5, 82));
        pntCs.add(new Point(35, 140));
        pntCs.add(new Point(39, 65));
        pntCs.add(new Point(117, 108));
        pntCs.add(new Point(100, 41));
        pntCs.add(new Point(159, 7));
        pntCs.add(new Point(101, -5));
        pntCs.add(new Point(160, -54));
        pntCs.add(new Point(121, -63));
        pntCs.add(new Point(126, -101));
        pntCs.add(new Point(41, -68));
        pntCs.add(new Point(27, -130));
        pntCs.add(new Point(-4, -62));
        pntCs.add(new Point(-45, -124));
        pntCs.add(new Point(-41, -64));
        pntCs.add(new Point(-109, -79));
        pntCs.add(new Point(-60, -42));
        pntCs.add(new Point(-135, 3));
        pntCs.add(new Point(-87, 35));
        pntCs.add(new Point(-90, 117));
        pntCs.add(new Point(-39, 23));
        pntCs.add(new Point(-34, 86));
        pntCs.add(new Point(2, 29));
        pntCs.add(new Point(23, -6));
        pntCs.add(new Point(35, 28));

        assignPolarPoints(pntCs);

        setColor(new Color(210, 206, 165));
        setExpire(10); // a hairball has limited range
        setRadius(10);

        // everything is relative to the falcon ship that fired the bullet
        setDeltaX(fal.getDeltaX() +
                Math.cos(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setDeltaY(fal.getDeltaY() +
                Math.sin(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setCenter(fal.getCenter());

        // set the bullet orientation to the falcon (ship) orientation
        setOrientation(fal.getLastOrientation());

    }

    @Override
    public void move() {

        super.move();
        if (getExpire() == 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            setExpire(getExpire() - 1);
        }
    }
}
