package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class Cruise extends Sprite {

    private final double FIRE_POWER = 15.0;
    private final int MAX_EXPIRE = 15;

    // for drawing alternative shapes
    // you could have more than one of these sets so that your sprite morphs into various shapes
    // throughout its life
    public double[] dLengthsAlts;
    public double[] dDegreesAlts;

    public Cruise(Falcon fal) {

        super();
        setTeam(Team.FRIEND);
        // defined the points on a cartesian grid
        ArrayList<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(-51, 168));
        pntCs.add(new Point(73, -152));
        pntCs.add(new Point(-54, 151));
        pntCs.add(new Point(37, -145));
        pntCs.add(new Point(6, 27));
        pntCs.add(new Point(-11, 151));
        pntCs.add(new Point(6, -107));
        pntCs.add(new Point(12, 9));
        pntCs.add(new Point(15, 114));
        pntCs.add(new Point(-27, -105));
        pntCs.add(new Point(18, 32));
        pntCs.add(new Point(47, 165));
        pntCs.add(new Point(2, 132));
        pntCs.add(new Point(-21, 96));
        pntCs.add(new Point(-40, -30));
        pntCs.add(new Point(54, -111));
        pntCs.add(new Point(-12, -7));
        pntCs.add(new Point(-21, 115));
        pntCs.add(new Point(-15, 172));
        pntCs.add(new Point(33, 155));
        pntCs.add(new Point(67, 85));
        pntCs.add(new Point(75, -4));
        pntCs.add(new Point(48, -67));
        pntCs.add(new Point(-22, -57));
        pntCs.add(new Point(-29, 97));
        pntCs.add(new Point(13, -24));
        pntCs.add(new Point(46, 99));
        pntCs.add(new Point(-20, 76));
        pntCs.add(new Point(-13, -56));
        pntCs.add(new Point(-30, -132));
        pntCs.add(new Point(44, -141));
        pntCs.add(new Point(7, -100));
        pntCs.add(new Point(7, -21));
        pntCs.add(new Point(65, -83));
        pntCs.add(new Point(19, -88));
        pntCs.add(new Point(-6, 8));
        pntCs.add(new Point(-29, 50));
        pntCs.add(new Point(31, 124));
        pntCs.add(new Point(58, 38));
        pntCs.add(new Point(65, -38));
        pntCs.add(new Point(17, -66));
        pntCs.add(new Point(-27, -93));
        pntCs.add(new Point(7, -149));
        assignPolarPoints(pntCs);


        // these are alt points
        ArrayList<Point> pntAs = new ArrayList<>();
        pntAs.add(new Point(-109, -7));
        pntAs.add(new Point(-32, 58));
        pntAs.add(new Point(30, -16));
        pntAs.add(new Point(100, 66));
        pntAs.add(new Point(149, 3));
        pntAs.add(new Point(68, 40));
        pntAs.add(new Point(89, -25));
        pntAs.add(new Point(30, 56));
        pntAs.add(new Point(-11, -24));
        pntAs.add(new Point(11, 57));
        pntAs.add(new Point(-42, 13));
        pntAs.add(new Point(-74, 49));
        pntAs.add(new Point(-68, -24));
        pntAs.add(new Point(-12, 101));
        pntAs.add(new Point(37, 142));
        pntAs.add(new Point(49, -52));
        pntAs.add(new Point(13, -107));
        pntAs.add(new Point(-27, -99));
        pntAs.add(new Point(-14, 48));
        pntAs.add(new Point(24, 103));
        pntAs.add(new Point(-23, 134));
        pntAs.add(new Point(-11, 38));
        pntAs.add(new Point(21, -66));
        pntAs.add(new Point(-8, -110));
        pntAs.add(new Point(-10, -62));
        pntAs.add(new Point(-37, -52));
        pntAs.add(new Point(55, 27));
        pntAs.add(new Point(33, 114));
        pntAs.add(new Point(50, 86));
        pntAs.add(new Point(115, 97));
        pntAs.add(new Point(75, 51));
        pntAs.add(new Point(19, 18));

        assignPolorPointsAlts(pntAs);

        // a cruise missile expires after 25 frames
        setExpire(MAX_EXPIRE);
        setRadius(20);

        // everything is relative to the falcon ship that fired the bullet
        setDeltaX(fal.getDeltaX()
                + Math.cos(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setDeltaY(fal.getDeltaY()
                + Math.sin(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setCenter(fal.getCenter());

        // set the bullet orientation to the falcon (ship) orientation
        setOrientation(fal.getOrientation());
        setColor(new Color(153, 84, 24));
    }


    // assign for alt image
    protected void assignPolorPointsAlts(ArrayList<Point> pntCs) {
        dDegreesAlts = convertToPolarDegs(pntCs);
        dLengthsAlts = convertToPolarLens(pntCs);
    }

    @Override
    public void move() {

        super.move();

        if (getExpire() < MAX_EXPIRE - 5) {
            setDeltaX(getDeltaX() * 1.07);
            setDeltaY(getDeltaY() * 1.07);
        }
        if (getExpire() == 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            setExpire(getExpire() - 1);
        }
    }

    @Override
    public void draw(Graphics g) {

        if (getExpire() < MAX_EXPIRE - 2) {
            super.draw(g);
            drawAlt(g);
        } else {
            drawAlt(g);
        }
    }

    public void drawAlt(Graphics g) {
        setXcoords(new int[dDegreesAlts.length]);
        setYcoords(new int[dDegreesAlts.length]);
        setObjectPoints(new Point[dDegrees.length]);

        for (int nC = 0; nC < dDegreesAlts.length; nC++) {

            setXcoord((int) (getCenter().x + getRadius()
                    * dLengthsAlts[nC]
                    * Math.sin(Math.toRadians(getOrientation()) + dDegreesAlts[nC])), nC);


            setYcoord((int) (getCenter().y - getRadius()
                    * dLengthsAlts[nC]
                    * Math.cos(Math.toRadians(getOrientation()) + dDegreesAlts[nC])), nC);
            // need this line of code to create the points which we will need for debris
            setObjectPoint(new Point(getXcoord(nC), getYcoord(nC)), nC);
        }
        g.setColor(new Color(204, 146, 65));
        g.drawPolygon(getXcoords(), getYcoords(), dDegreesAlts.length);
    }
}
