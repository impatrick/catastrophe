package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;

public class DebrisClogged implements Movable {

    private int mExpiry;
    private Point mPoint;

    public DebrisClogged(int mEmpiry, Point mPoint) {
        this.mExpiry = mEmpiry;
        this.mPoint = mPoint;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(new Color(
                128,
                Game.R.nextInt(128),
                Game.R.nextInt(20)
        ));
        // the enclosing rectangle gets smaller and smaller
        g.fillOval(mPoint.x, mPoint.y, mExpiry, mExpiry);
    }

    @Override
    public void move() {
        if (mExpiry == 0) {
            CommandCenter.getInstance().getOpsList()
                    .enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            mExpiry--;
        }
    }

    // collision detection
    @Override
    public Point getCenter() {
        return null;
    }

    @Override
    public int getRadius() {
        return 0;
    }

    @Override
    public Team getTeam() {
        return Team.DEBRIS;
    }
}
