package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;


public class Falcon extends Sprite {

    public static final int SPEED = 25;

    // reserved for potential use
    // private boolean bShield = false;
    private boolean bFlame = true;
    private boolean bProtected; // for fade in and out
    private int nLastOrientation;

    private final double[] FLAME = {23 * Math.PI / 24 + Math.PI / 2,
            Math.PI + Math.PI / 2, 25 * Math.PI / 24 + Math.PI / 2};

    private int[] nXFlames = new int[FLAME.length];
    private int[] nYFlames = new int[FLAME.length];
    private Point[] pntFlames = new Point[FLAME.length];

    public Falcon() {
        super();
        setTeam(Team.FRIEND);

        assignPolarPoints(modelCatRight());

        setColor(Color.WHITE);

        // put falcon in the middle
        setCenter(new Point(Game.DIM.width / 2, Game.DIM.height / 2));

        // this is the size of the falcon
        setRadius(35);

        // these are falcon specific
        setProtected(true);
        setFadeValue(0);
    }

    public Falcon(Color color) { // constructor with cat drawn for lives
        super();
        setTeam(Team.FRIEND);

        assignPolarPoints(modelCatUp());

        setColor(color);

    }

    @Override
    public void move() {
        super.move(STICKS, STICKS);

        // implementing the fadeInOut functionality - added by Dmitriy
        if (getProtected()) {
            setFadeValue(getFadeValue() + 3);
        }
        if (getFadeValue() == 255) {
            setProtected(false);
        }
    }

    public void move(int nDir) {
        switch (nDir) {
            case UP:
                setDeltaY(-SPEED);
                assignPolarPoints(modelCatUp());
                setOrientation(UP);
                bFlame = true;
                break;
            case DOWN:
                setDeltaY(SPEED);
                assignPolarPoints(modelCatDown());
                setOrientation(DOWN);
                bFlame = true;
                break;
            case LEFT:
                setDeltaX(-SPEED);
                assignPolarPoints(modelCatLeft());
                setOrientation(LEFT);
                bFlame = true;
                break;
            case RIGHT:
                setDeltaX(SPEED);
                assignPolarPoints(modelCatRight());
                setOrientation(RIGHT);
                bFlame = true;
                break;
            default:
                break;
        }
    }

    public void stopMove(int nDir) {
        switch (nDir) {
            case UP:
                setDeltaY(0);
                bFlame = false;
                break;
            case DOWN:
                setDeltaY(0);
                bFlame = false;
                break;
            case LEFT:
                setDeltaX(0);
                bFlame = false;
                break;
            case RIGHT:
                setDeltaX(0);
                bFlame = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void draw(Graphics g) {

        super.draw(g);

        Color colShip = Color.WHITE;
        g.fillPolygon(getXcoords(), getYcoords(), dDegrees.length);

        // thrusting
        if (bFlame) {
            g.setColor(colShip);
            // the flame
            for (int nC = 0; nC < FLAME.length; nC++) {
                if (nC % 2 != 0) //odd
                {
                    pntFlames[nC] = new Point((int) (getCenter().x + 2
                            * getRadius()
                            * Math.sin(Math.toRadians(getOrientation())
                            + FLAME[nC])), (int) (getCenter().y - 2
                            * getRadius()
                            * Math.cos(Math.toRadians(getOrientation())
                            + FLAME[nC])));

                } else // even
                {
                    pntFlames[nC] = new Point((int) (getCenter().x + getRadius()
                            * 1.1
                            * Math.sin(Math.toRadians(getOrientation())
                            + FLAME[nC])),
                            (int) (getCenter().y - getRadius()
                                    * 1.1
                                    * Math.cos(Math.toRadians(getOrientation())
                                    + FLAME[nC])));

                } // end even/odd else

            } // end for loop

            for (int nC = 0; nC < FLAME.length; nC++) {
                nXFlames[nC] = pntFlames[nC].x;
                nYFlames[nC] = pntFlames[nC].y;

            } // end assign flame points

            // g.setColor(Color.white);
            g.fillPolygon(nXFlames, nYFlames, FLAME.length);

            // special debris graphic effects if cat is invincible
            if (getExpire() != 0) {
                bProtected = true;
                debrisTrail(this);
                setExpire(getExpire() - 1);
                if (getExpire() <= 1) {
                    bProtected = false;
                    setColor(Color.WHITE);
                }
            }
            if (bProtected && getExpire() != 0) {
                setColor(Color.GREEN);
            }
            if (Game.getMissileStatus()) {
                setColor(Color.RED);
            }
            if (Game.getMissileStatus() && bProtected) {
                setColor(Color.MAGENTA);
            }
            if (!bProtected && !Game.getMissileStatus()) {
                setColor(Color.WHITE);
            }
            //else {
            //    setColor(Color.WHITE);
            //}
        }
    }

    public void setProtected(boolean bParam) {
        if (bParam) {
            setFadeValue(0);
        }
        bProtected = bParam;
    }

    public boolean getProtected() {
        return bProtected;
    }

    private static void debrisTrail(Movable movable) {
        Point debrisPoint;
        if (movable instanceof Sprite) {
            for (int nC = 0; nC < 360; nC = nC + 60) {
                debrisPoint = new Point(movable.getCenter().x + Game.R.nextInt(60), movable.getCenter().y + Game.R.nextInt(60));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(15, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x - Game.R.nextInt(60), movable.getCenter().y - Game.R.nextInt(60));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(15, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x + Game.R.nextInt(60), movable.getCenter().y - Game.R.nextInt(60));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(15, debrisPoint),
                        CollisionOp.Operation.ADD);
                debrisPoint = new Point(movable.getCenter().x - Game.R.nextInt(60), movable.getCenter().y + Game.R.nextInt(60));
                CommandCenter.getInstance().getOpsList().enqueue(new Debris(15, debrisPoint),
                        CollisionOp.Operation.ADD);
            }
        } else {
            return;
        }
    }

    public void setLastOrientation(int nLastOrientation) {
        this.nLastOrientation = nLastOrientation;

    }

    public int getLastOrientation() {
        return nLastOrientation;
    }

    private ArrayList<Point> modelCatRight() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(49, -206));
        pntCs.add(new Point(59, -217));
        pntCs.add(new Point(75, -232));
        pntCs.add(new Point(94, -256));
        pntCs.add(new Point(124, -271));
        pntCs.add(new Point(150, -290));
        pntCs.add(new Point(179, -307));
        pntCs.add(new Point(207, -319));
        pntCs.add(new Point(227, -333));
        pntCs.add(new Point(237, -347));
        pntCs.add(new Point(248, -359));
        pntCs.add(new Point(259, -366));
        pntCs.add(new Point(266, -362));
        pntCs.add(new Point(266, -348));
        pntCs.add(new Point(263, -332));
        pntCs.add(new Point(261, -317));
        pntCs.add(new Point(247, -304));
        pntCs.add(new Point(236, -297));
        pntCs.add(new Point(209, -285));
        pntCs.add(new Point(187, -270));
        pntCs.add(new Point(156, -253));
        pntCs.add(new Point(119, -225));
        pntCs.add(new Point(91, -193));
        pntCs.add(new Point(82, -151));
        pntCs.add(new Point(82, -96));
        pntCs.add(new Point(73, -32));
        pntCs.add(new Point(67, 30));
        pntCs.add(new Point(62, 86));
        pntCs.add(new Point(70, 141));
        pntCs.add(new Point(78, 185));
        pntCs.add(new Point(73, 234));
        pntCs.add(new Point(91, 271));
        pntCs.add(new Point(106, 292));
        pntCs.add(new Point(129, 307));
        pntCs.add(new Point(133, 317));
        pntCs.add(new Point(115, 313));
        pntCs.add(new Point(130, 334));
        pntCs.add(new Point(95, 330));
        pntCs.add(new Point(81, 341));
        pntCs.add(new Point(65, 353));
        pntCs.add(new Point(26, 369));
        pntCs.add(new Point(14, 362));
        pntCs.add(new Point(-4, 352));
        pntCs.add(new Point(-4, 337));
        pntCs.add(new Point(-2, 309));
        pntCs.add(new Point(-6, 289));
        pntCs.add(new Point(-14, 271));
        pntCs.add(new Point(-51, 245));
        pntCs.add(new Point(-76, 227));
        pntCs.add(new Point(-100, 217));
        pntCs.add(new Point(-118, 227));
        pntCs.add(new Point(-134, 246));
        pntCs.add(new Point(-158, 266));
        pntCs.add(new Point(-180, 279));
        pntCs.add(new Point(-212, 289));
        pntCs.add(new Point(-228, 296));
        pntCs.add(new Point(-257, 291));
        pntCs.add(new Point(-252, 272));
        pntCs.add(new Point(-233, 263));
        pntCs.add(new Point(-211, 253));
        pntCs.add(new Point(-182, 242));
        pntCs.add(new Point(-169, 226));
        pntCs.add(new Point(-133, 168));
        pntCs.add(new Point(-203, 164));
        pntCs.add(new Point(-215, 170));
        pntCs.add(new Point(-227, 180));
        pntCs.add(new Point(-235, 191));
        pntCs.add(new Point(-244, 197));
        pntCs.add(new Point(-254, 194));
        pntCs.add(new Point(-256, 176));
        pntCs.add(new Point(-254, 152));
        pntCs.add(new Point(-238, 143));
        pntCs.add(new Point(-206, 133));
        pntCs.add(new Point(-150, 124));
        pntCs.add(new Point(-105, 109));
        pntCs.add(new Point(-96, 103));
        pntCs.add(new Point(-100, 88));
        pntCs.add(new Point(-101, 52));
        pntCs.add(new Point(-101, 24));
        pntCs.add(new Point(-106, 10));
        pntCs.add(new Point(-108, -28));
        pntCs.add(new Point(-97, -51));
        pntCs.add(new Point(-112, -65));
        pntCs.add(new Point(-135, -83));
        pntCs.add(new Point(-161, -101));
        pntCs.add(new Point(-183, -113));
        pntCs.add(new Point(-203, -104));
        pntCs.add(new Point(-219, -92));
        pntCs.add(new Point(-230, -77));
        pntCs.add(new Point(-234, -67));
        pntCs.add(new Point(-238, -54));
        pntCs.add(new Point(-246, -42));
        pntCs.add(new Point(-259, -37));
        pntCs.add(new Point(-264, -51));
        pntCs.add(new Point(-264, -71));
        pntCs.add(new Point(-258, -94));
        pntCs.add(new Point(-241, -115));
        pntCs.add(new Point(-216, -138));
        pntCs.add(new Point(-195, -152));
        pntCs.add(new Point(-186, -158));
        pntCs.add(new Point(-175, -155));
        pntCs.add(new Point(-154, -151));
        pntCs.add(new Point(-141, -151));
        pntCs.add(new Point(-105, -149));
        pntCs.add(new Point(-117, -168));
        pntCs.add(new Point(-130, -189));
        pntCs.add(new Point(-140, -214));
        pntCs.add(new Point(-155, -240));
        pntCs.add(new Point(-174, -258));
        pntCs.add(new Point(-191, -272));
        pntCs.add(new Point(-210, -273));
        pntCs.add(new Point(-232, -266));
        pntCs.add(new Point(-239, -260));
        pntCs.add(new Point(-247, -255));
        pntCs.add(new Point(-252, -258));
        pntCs.add(new Point(-255, -267));
        pntCs.add(new Point(-253, -282));
        pntCs.add(new Point(-243, -297));
        pntCs.add(new Point(-229, -301));
        pntCs.add(new Point(-181, -300));
        pntCs.add(new Point(-148, -296));
        pntCs.add(new Point(-135, -280));
        pntCs.add(new Point(-114, -264));
        pntCs.add(new Point(-88, -245));
        pntCs.add(new Point(-55, -225));
        pntCs.add(new Point(-26, -217));
        pntCs.add(new Point(9, -215));
        pntCs.add(new Point(29, -205));

        return pntCs;
    }

    private ArrayList<Point> modelCatLeft() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(-25, 369));
        pntCs.add(new Point(-55, 354));
        pntCs.add(new Point(-71, 349));
        pntCs.add(new Point(-90, 331));
        pntCs.add(new Point(-112, 332));
        pntCs.add(new Point(-128, 335));
        pntCs.add(new Point(-113, 314));
        pntCs.add(new Point(-129, 314));
        pntCs.add(new Point(-108, 297));
        pntCs.add(new Point(-100, 288));
        pntCs.add(new Point(-83, 268));
        pntCs.add(new Point(-73, 242));
        pntCs.add(new Point(-65, 221));
        pntCs.add(new Point(-72, 182));
        pntCs.add(new Point(-65, 148));
        pntCs.add(new Point(-56, 116));
        pntCs.add(new Point(-58, 60));
        pntCs.add(new Point(-64, 15));
        pntCs.add(new Point(-71, -45));
        pntCs.add(new Point(-78, -98));
        pntCs.add(new Point(-79, -157));
        pntCs.add(new Point(-88, -191));
        pntCs.add(new Point(-112, -220));
        pntCs.add(new Point(-136, -239));
        pntCs.add(new Point(-163, -261));
        pntCs.add(new Point(-195, -281));
        pntCs.add(new Point(-228, -297));
        pntCs.add(new Point(-257, -317));
        pntCs.add(new Point(-261, -339));
        pntCs.add(new Point(-258, -366));
        pntCs.add(new Point(-245, -368));
        pntCs.add(new Point(-232, -343));
        pntCs.add(new Point(-199, -320));
        pntCs.add(new Point(-176, -302));
        pntCs.add(new Point(-161, -301));
        pntCs.add(new Point(-133, -284));
        pntCs.add(new Point(-116, -272));
        pntCs.add(new Point(-82, -245));
        pntCs.add(new Point(-42, -204));
        pntCs.add(new Point(-9, -213));
        pntCs.add(new Point(21, -215));
        pntCs.add(new Point(57, -223));
        pntCs.add(new Point(94, -244));
        pntCs.add(new Point(111, -255));
        pntCs.add(new Point(128, -272));
        pntCs.add(new Point(158, -296));
        pntCs.add(new Point(203, -302));
        pntCs.add(new Point(246, -295));
        pntCs.add(new Point(256, -279));
        pntCs.add(new Point(257, -262));
        pntCs.add(new Point(254, -254));
        pntCs.add(new Point(235, -265));
        pntCs.add(new Point(214, -270));
        pntCs.add(new Point(184, -262));
        pntCs.add(new Point(169, -246));
        pntCs.add(new Point(148, -219));
        pntCs.add(new Point(130, -184));
        pntCs.add(new Point(114, -152));
        pntCs.add(new Point(148, -151));
        pntCs.add(new Point(180, -157));
        pntCs.add(new Point(203, -156));
        pntCs.add(new Point(224, -133));
        pntCs.add(new Point(251, -111));
        pntCs.add(new Point(263, -90));
        pntCs.add(new Point(265, -47));
        pntCs.add(new Point(258, -39));
        pntCs.add(new Point(242, -56));
        pntCs.add(new Point(230, -83));
        pntCs.add(new Point(189, -107));
        pntCs.add(new Point(156, -91));
        pntCs.add(new Point(126, -76));
        pntCs.add(new Point(104, -53));
        pntCs.add(new Point(109, -29));
        pntCs.add(new Point(113, 1));
        pntCs.add(new Point(107, 29));
        pntCs.add(new Point(103, 63));
        pntCs.add(new Point(104, 98));
        pntCs.add(new Point(112, 112));
        pntCs.add(new Point(142, 124));
        pntCs.add(new Point(172, 132));
        pntCs.add(new Point(213, 135));
        pntCs.add(new Point(241, 146));
        pntCs.add(new Point(256, 157));
        pntCs.add(new Point(259, 184));
        pntCs.add(new Point(255, 198));
        pntCs.add(new Point(235, 187));
        pntCs.add(new Point(220, 167));
        pntCs.add(new Point(137, 167));
        pntCs.add(new Point(176, 234));
        pntCs.add(new Point(213, 254));
        pntCs.add(new Point(238, 265));
        pntCs.add(new Point(251, 276));
        pntCs.add(new Point(260, 293));
        pntCs.add(new Point(230, 295));
        pntCs.add(new Point(206, 287));
        pntCs.add(new Point(175, 271));
        pntCs.add(new Point(143, 250));
        pntCs.add(new Point(108, 218));
        pntCs.add(new Point(88, 223));
        pntCs.add(new Point(56, 242));
        pntCs.add(new Point(34, 262));
        pntCs.add(new Point(11, 284));
        pntCs.add(new Point(1, 309));
        pntCs.add(new Point(9, 333));
        pntCs.add(new Point(-3, 359));

        return pntCs;
    }

    private ArrayList<Point> modelCatUp() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(-200, 15));
        pntCs.add(new Point(-209, 27));
        pntCs.add(new Point(-207, 5));
        pntCs.add(new Point(-230, 26));
        pntCs.add(new Point(-223, -12));
        pntCs.add(new Point(-247, -43));
        pntCs.add(new Point(-260, -69));
        pntCs.add(new Point(-262, -83));
        pntCs.add(new Point(-245, -104));
        pntCs.add(new Point(-218, -105));
        pntCs.add(new Point(-163, -116));
        pntCs.add(new Point(-134, -152));
        pntCs.add(new Point(-125, -179));
        pntCs.add(new Point(-112, -208));
        pntCs.add(new Point(-129, -226));
        pntCs.add(new Point(-155, -249));
        pntCs.add(new Point(-179, -287));
        pntCs.add(new Point(-189, -325));
        pntCs.add(new Point(-188, -356));
        pntCs.add(new Point(-167, -349));
        pntCs.add(new Point(-156, -322));
        pntCs.add(new Point(-132, -282));
        pntCs.add(new Point(-111, -268));
        pntCs.add(new Point(-60, -236));
        pntCs.add(new Point(-60, -299));
        pntCs.add(new Point(-61, -323));
        pntCs.add(new Point(-93, -353));
        pntCs.add(new Point(-68, -357));
        pntCs.add(new Point(-43, -354));
        pntCs.add(new Point(-31, -323));
        pntCs.add(new Point(-24, -297));
        pntCs.add(new Point(-18, -251));
        pntCs.add(new Point(-12, -215));
        pntCs.add(new Point(3, -201));
        pntCs.add(new Point(38, -201));
        pntCs.add(new Point(84, -207));
        pntCs.add(new Point(113, -208));
        pntCs.add(new Point(151, -205));
        pntCs.add(new Point(175, -231));
        pntCs.add(new Point(202, -257));
        pntCs.add(new Point(214, -283));
        pntCs.add(new Point(207, -310));
        pntCs.add(new Point(191, -332));
        pntCs.add(new Point(178, -343));
        pntCs.add(new Point(151, -354));
        pntCs.add(new Point(152, -367));
        pntCs.add(new Point(178, -367));
        pntCs.add(new Point(209, -359));
        pntCs.add(new Point(228, -336));
        pntCs.add(new Point(244, -312));
        pntCs.add(new Point(256, -297));
        pntCs.add(new Point(260, -266));
        pntCs.add(new Point(253, -228));
        pntCs.add(new Point(255, -215));
        pntCs.add(new Point(292, -234));
        pntCs.add(new Point(341, -249));
        pntCs.add(new Point(363, -269));
        pntCs.add(new Point(379, -293));
        pntCs.add(new Point(375, -322));
        pntCs.add(new Point(364, -345));
        pntCs.add(new Point(364, -359));
        pntCs.add(new Point(383, -359));
        pntCs.add(new Point(400, -349));
        pntCs.add(new Point(406, -328));
        pntCs.add(new Point(403, -254));
        pntCs.add(new Point(390, -232));
        pntCs.add(new Point(363, -211));
        pntCs.add(new Point(340, -174));
        pntCs.add(new Point(329, -146));
        pntCs.add(new Point(324, -111));
        pntCs.add(new Point(313, -60));
        pntCs.add(new Point(328, -38));
        pntCs.add(new Point(350, -16));
        pntCs.add(new Point(370, 3));
        pntCs.add(new Point(387, 34));
        pntCs.add(new Point(408, 61));
        pntCs.add(new Point(425, 99));
        pntCs.add(new Point(446, 124));
        pntCs.add(new Point(467, 144));
        pntCs.add(new Point(464, 162));
        pntCs.add(new Point(430, 156));
        pntCs.add(new Point(409, 144));
        pntCs.add(new Point(387, 91));
        pntCs.add(new Point(353, 48));
        pntCs.add(new Point(318, -2));
        pntCs.add(new Point(278, -20));
        pntCs.add(new Point(221, -27));
        pntCs.add(new Point(187, -23));
        pntCs.add(new Point(144, -30));
        pntCs.add(new Point(104, -33));
        pntCs.add(new Point(55, -37));
        pntCs.add(new Point(13, -40));
        pntCs.add(new Point(-29, -35));
        pntCs.add(new Point(-57, -26));
        pntCs.add(new Point(-91, -31));
        pntCs.add(new Point(-133, -30));
        pntCs.add(new Point(-165, -10));
        pntCs.add(new Point(-182, -2));

        return pntCs;
    }

    private ArrayList<Point> modelCatDown() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(-269, 253));
        pntCs.add(new Point(-254, 213));
        pntCs.add(new Point(-243, 191));
        pntCs.add(new Point(-220, 165));
        pntCs.add(new Point(-182, 146));
        pntCs.add(new Point(-165, 139));
        pntCs.add(new Point(-163, 176));
        pntCs.add(new Point(-159, 201));
        pntCs.add(new Point(-172, 231));
        pntCs.add(new Point(-187, 242));
        pntCs.add(new Point(-199, 258));
        pntCs.add(new Point(-179, 261));
        pntCs.add(new Point(-145, 252));
        pntCs.add(new Point(-142, 230));
        pntCs.add(new Point(-135, 203));
        pntCs.add(new Point(-134, 159));
        pntCs.add(new Point(-117, 124));
        pntCs.add(new Point(-111, 108));
        pntCs.add(new Point(-98, 99));
        pntCs.add(new Point(-45, 102));
        pntCs.add(new Point(-19, 111));
        pntCs.add(new Point(18, 108));
        pntCs.add(new Point(58, 99));
        pntCs.add(new Point(75, 122));
        pntCs.add(new Point(95, 144));
        pntCs.add(new Point(104, 170));
        pntCs.add(new Point(109, 192));
        pntCs.add(new Point(100, 214));
        pntCs.add(new Point(86, 232));
        pntCs.add(new Point(70, 243));
        pntCs.add(new Point(54, 247));
        pntCs.add(new Point(53, 254));
        pntCs.add(new Point(61, 262));
        pntCs.add(new Point(94, 261));
        pntCs.add(new Point(122, 243));
        pntCs.add(new Point(152, 219));
        pntCs.add(new Point(160, 197));
        pntCs.add(new Point(152, 164));
        pntCs.add(new Point(152, 108));
        pntCs.add(new Point(175, 125));
        pntCs.add(new Point(214, 137));
        pntCs.add(new Point(237, 147));
        pntCs.add(new Point(262, 163));
        pntCs.add(new Point(272, 186));
        pntCs.add(new Point(275, 209));
        pntCs.add(new Point(264, 236));
        pntCs.add(new Point(253, 258));
        pntCs.add(new Point(269, 259));
        pntCs.add(new Point(293, 248));
        pntCs.add(new Point(303, 231));
        pntCs.add(new Point(303, 203));
        pntCs.add(new Point(300, 163));
        pntCs.add(new Point(289, 135));
        pntCs.add(new Point(265, 108));
        pntCs.add(new Point(234, 60));
        pntCs.add(new Point(211, 9));
        pntCs.add(new Point(209, -30));
        pntCs.add(new Point(208, -54));
        pntCs.add(new Point(238, -77));
        pntCs.add(new Point(259, -111));
        pntCs.add(new Point(297, -151));
        pntCs.add(new Point(317, -200));
        pntCs.add(new Point(332, -226));
        pntCs.add(new Point(365, -240));
        pntCs.add(new Point(365, -258));
        pntCs.add(new Point(348, -262));
        pntCs.add(new Point(328, -260));
        pntCs.add(new Point(311, -247));
        pntCs.add(new Point(298, -224));
        pntCs.add(new Point(288, -202));
        pntCs.add(new Point(264, -171));
        pntCs.add(new Point(251, -148));
        pntCs.add(new Point(224, -115));
        pntCs.add(new Point(198, -94));
        pntCs.add(new Point(179, -85));
        pntCs.add(new Point(156, -82));
        pntCs.add(new Point(114, -82));
        pntCs.add(new Point(97, -74));
        pntCs.add(new Point(52, -66));
        pntCs.add(new Point(2, -68));
        pntCs.add(new Point(-78, -59));
        pntCs.add(new Point(-153, -75));
        pntCs.add(new Point(-187, -69));
        pntCs.add(new Point(-237, -75));
        pntCs.add(new Point(-266, -94));
        pntCs.add(new Point(-288, -105));
        pntCs.add(new Point(-312, -134));
        pntCs.add(new Point(-311, -112));
        pntCs.add(new Point(-338, -131));
        pntCs.add(new Point(-328, -93));
        pntCs.add(new Point(-344, -65));
        pntCs.add(new Point(-367, -38));
        pntCs.add(new Point(-363, -24));
        pntCs.add(new Point(-354, -4));
        pntCs.add(new Point(-316, 0));
        pntCs.add(new Point(-294, -1));
        pntCs.add(new Point(-277, 9));
        pntCs.add(new Point(-253, 23));
        pntCs.add(new Point(-235, 62));
        pntCs.add(new Point(-222, 97));
        pntCs.add(new Point(-222, 124));
        pntCs.add(new Point(-244, 141));
        pntCs.add(new Point(-259, 159));
        pntCs.add(new Point(-271, 178));
        pntCs.add(new Point(-283, 205));
        pntCs.add(new Point(-292, 237));
        pntCs.add(new Point(-293, 256));

        return pntCs;
    }


}
