package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class Litterbox extends Sprite {

    public Litterbox() {

        // this was formerly an asteroid
        super();
        ArrayList<Point> pntCs = new ArrayList<Point>();
        pntCs.add(new Point(-52, -129));
        pntCs.add(new Point(-64, -97));
        pntCs.add(new Point(-64, 98));
        pntCs.add(new Point(-51, 135));
        pntCs.add(new Point(52, 152));
        pntCs.add(new Point(52, 170));
        pntCs.add(new Point(76, 165));
        pntCs.add(new Point(61, 74));
        pntCs.add(new Point(46, 19));
        pntCs.add(new Point(40, -22));
        pntCs.add(new Point(42, -76));
        pntCs.add(new Point(36, -117));
        pntCs.add(new Point(27, -146));
        pntCs.add(new Point(-17, -167));
        pntCs.add(new Point(-4, -157));
        pntCs.add(new Point(20, -135));
        pntCs.add(new Point(24, -50));
        pntCs.add(new Point(31, 5));
        pntCs.add(new Point(44, 61));
        pntCs.add(new Point(53, 143));
        pntCs.add(new Point(44, 152));
        pntCs.add(new Point(-38, 136));
        pntCs.add(new Point(-57, 95));
        pntCs.add(new Point(-56, -99));
        pntCs.add(new Point(-42, -126));
        pntCs.add(new Point(-7, -133));
        pntCs.add(new Point(-4, -148));
        pntCs.add(new Point(-9, -141));
        pntCs.add(new Point(-19, -132));
        pntCs.add(new Point(-42, -135));

        assignPolarPoints(pntCs);
        setRadius(60);
        setColor(Color.WHITE);

        setTeam(Team.FOE);

    }
}
