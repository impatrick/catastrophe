package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

public class MouseFloater extends Sprite implements Scorable {

    public MouseFloater() {

        super();
        setTeam(Team.FOE); // mouse is a foe
        ArrayList<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(5, 5));
        pntCs.add(new Point(4, 0));
        pntCs.add(new Point(5, -5));
        pntCs.add(new Point(0, -4));
        pntCs.add(new Point(-5, -5));
        pntCs.add(new Point(-4, 0));
        pntCs.add(new Point(-5, 5));
        pntCs.add(new Point(0, 4));

        assignPolarPoints(pntCs);

        setExpire(250);
        setRadius(50);
        setColor(Color.RED);
        setCenter(spawnLocation()); // gets pseudo random spawn location

        // random orientation
        setOrientation(Game.R.nextInt(360));
    }

    private void setRandomDirection() {
        int nX = Game.R.nextInt(10);
        int nY = Game.R.nextInt(10);

        //set random DeltaX
        if (nX % 2 == 0) {
            setDeltaX(nX);
        } else {
            setDeltaX(-nX);
        }

        //set random DeltaY
        if (nY % 2 == 0) {
            setDeltaY(nY);
        } else {
            setDeltaY(-nY);
        }
    }

    private Point spawnLocation() { // returns a pseudo random spawn location
        Point spawnLocation = null;
        switch (Game.nSpawnCorner) {
            case 0:
                spawnLocation = new Point(((int) Game.DIM.getWidth()),
                        Game.R.nextInt(Game.DIM.height));
            case 1:
                spawnLocation = new Point(((int) Game.DIM.getWidth()),
                        Game.R.nextInt(Game.DIM.height));
            case 2:
                spawnLocation = new Point(1,
                        Game.R.nextInt(Game.DIM.height));
            case 3:
                spawnLocation = new Point(1,
                        Game.R.nextInt(Game.DIM.height));
            default:
                break;
        }
        return spawnLocation;
    }

    @Override
    public void move() {
        super.move(LOOPS, LOOPS); // the mouse can go through walls of course

        // adding expire functionality
        if (getExpire() == 0) {
            Game.spawnNewMouse();
        } else {
            if (getExpire() % 20 == 0) {
                setRandomDirection();
            }
            setExpire(getExpire() - 1);
        }
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        // fill this polygon (with whatever color it has)
        g.fillPolygon(getXcoords(), getYcoords(), dDegrees.length);
        // now draw a white border
        g.setColor(Color.WHITE);
        g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
    }

    @Override
    public int getScore() {
        return 100; // a mouse is worth 100 points
    }
}
