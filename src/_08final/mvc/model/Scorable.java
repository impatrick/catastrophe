package _08final.mvc.model;

/**
 * Created by patricklau on 12/1/16.
 */
public interface Scorable {
    int getScore();

}
