package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

public abstract class Sprite implements Movable {
    // added code in next two lines for orientation and frame collision
    public static final int RIGHT = 0, DOWN = 90, LEFT = 180, UP = 270; // sets degrees for orientations
    public static final int LOOPS = 1, STICKS = 2, LEAVES = 3, STRONGSTICK = 4; // sets codes for the move method

    public final int STANDARD_RADIUS = 10;

    private Point pntCenter; // the center-point of this sprite
    private double dDeltaX, dDeltaY; // this causes movement; change in x and change in y

    // every sprite needs to know about the size of the gaming environment
    private Dimension dim; // dim of the gaming environment

    private Team mTeam; // we need to know what team we're on
    private int nRadius; // the radius of circumscribing circle

    private int nOrientation;
    private int nExpiry; // natural mortality (short-living objects)
    private Color col; // the color of this sprite

    // radial coordinates
    // this game uses radial coordinates to render sprites
    public double[] dLengths;
    public double[] dDegrees;

    private int nFade; // fade value for fading in and out

    // these are used to draw the polygon. You don't usually need to interface with these
    private Point[] pntCoords; // an array of points used to draw polygon
    private int[] nXCoords;
    private int[] nYCoords;

    private boolean bFull; // is it drawn filled in
    private boolean bOffScreen; // is it off screen

    @Override
    public Team getTeam() {
        //default
        return mTeam;
    }

    public void setTeam(Team team) {
        mTeam = team;
    }

    // modified the move method to interact with boundaries X and Y

    public void move(int nXCode, int nYCode) {

        Point pnt = getCenter();
        double dX = pnt.x + getDeltaX();
        double dY = pnt.y + getDeltaY();

        if (pnt.x > getDim().width) { // behavior when hitting right boundary
            switch (nXCode) {
                case LOOPS:
                    setCenter(new Point(1, pnt.y));
                    break;
                case STICKS:
                    setCenter(new Point(getDim().width - 1, pnt.y + 1));
                    // vacuums will turn when they hit a wall
                    if (this instanceof Vacuum) {
                        ((Vacuum) this).turn();
                    }
                    break;
                case LEAVES:
                    bOffScreen = true;
                    break;
            }
        } else if (pnt.x < 0) { // behavior when hitting left boundary
            switch (nXCode) {
                case LOOPS:
                    setCenter(new Point(getDim().width - 1, pnt.y + 1));
                    break;
                case STICKS:
                    setCenter(new Point(1, pnt.y));
                    if (this instanceof Vacuum) {
                        ((Vacuum) this).turn();
                    }
                    break;
                case LEAVES:
                    bOffScreen = true;
                    break;
            }
        } else if (pnt.y < 0) { // behavior when hitting top boundary
            switch (nYCode) {
                case LOOPS:
                    setCenter(new Point(pnt.x, getDim().height - 1));
                    break;
                case STICKS:
                    setCenter(new Point(pnt.x, 1));
                    break;
                case LEAVES:
                    bOffScreen = true;
                    break;
            }
        } else if (pnt.y > getDim().height) { // behavior when hitting bottom boundary
            switch (nYCode) {
                case LOOPS:
                    setCenter(new Point(pnt.x, 1));
                    break;
                case STICKS:
                    setCenter(new Point(pnt.x, getDim().height - 1));
                    break;
                case LEAVES:
                    bOffScreen = true;
                    break;
            }
        } else {
            setCenter(new Point((int) dX, (int) dY));
        }
    }

    // default move is to go off screen
    public void move() {
        move(STICKS, STICKS);
    }

    public Sprite() {

        // you can override this and many more in the subclasses
        setDim(Game.DIM);
        setColor(Color.white);
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
    }

    public void setExpire(int n) {
        nExpiry = n;
    }

    public double[] getLengths() {
        return this.dLengths;
    }

    public void setLengths(double[] dLengths) {
        this.dLengths = dLengths;
    }

    public double[] getDegrees() {
        return this.dDegrees;
    }

    public void setDegrees(double[] dDegrees) {
        this.dDegrees = dDegrees;
    }

    public Color getColor() {
        return col;
    }

    public void setColor(Color col) {
        this.col = col;
    }

    public int points() {
        return 0; //default is zero
    }

    public int getExpire() {
        return nExpiry;
    }

    public int getOrientation() {
        return nOrientation;
    }

    public void setOrientation(int n) {
        nOrientation = n;
    }

    public void setDeltaX(double dSet) {
        dDeltaX = dSet;
    }

    public void setDeltaY(double dSet) {
        dDeltaY = dSet;
    }

    public double getDeltaY() {
        return dDeltaY;
    }

    public double getDeltaX() {
        return dDeltaX;
    }

    public int getRadius() {
        return nRadius;
    }

    public void setRadius(int n) {
        nRadius = n;
    }

    public Dimension getDim() {
        return dim;
    }

    public void setDim(Dimension dim) {
        this.dim = dim;
    }

    public Point getCenter() {
        return pntCenter;
    }

    public void setCenter(Point pntParam) {
        pntCenter = pntParam;
    }

    public void setYcoord(int nValue, int nIndex) {
        nYCoords[nIndex] = nValue;
    }

    public void setXcoord(int nValue, int nIndex) {
        nXCoords[nIndex] = nValue;
    }

    public int getYcoord(int nIndex) {
        return nYCoords[nIndex];// = nValue;
    }

    public int getXcoord(int nIndex) {
        return nXCoords[nIndex];// = nValue;
    }

    public int[] getXcoords() {
        return nXCoords;
    }

    public int[] getYcoords() {
        return nYCoords;
    }

    public void setXcoords(int[] nCoords) {
        nXCoords = nCoords;
    }

    public void setYcoords(int[] nCoords) {
        nYCoords = nCoords;
    }

    protected double hypot(double dX, double dY) {
        return Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
    }

    // utility function to convert from cartesian to polar
    // since it's much easier to describe a sprite as a list of cartesian points
    // sprites (except Litterbox) should use the cartesian technique to describe the coordinates
    // see Falcon or Bullet constructor for examples
    protected double[] convertToPolarDegs(ArrayList<Point> pntPoints) {

        double[] dDegs = new double[pntPoints.size()];

        int nC = 0;
        for (Point pnt : pntPoints) {
            dDegs[nC++] = (Math.toDegrees(Math.atan2(pnt.y, pnt.x))) * Math.PI / 180;
        }
        return dDegs;
    }

    // utility function to convert to polar
    protected double[] convertToPolarLens(ArrayList<Point> pntPoints) {

        double[] dLens = new double[pntPoints.size()];

        // determine the largest hypotenuse
        double dL = 0;
        for (Point pnt : pntPoints) {
            if (hypot(pnt.x, pnt.y) > dL) {
                dL = hypot(pnt.x, pnt.y);
            }
        }

        int nC = 0;
        for (Point pnt : pntPoints) {
            if (pnt.x == 0 && pnt.y > 0) {
                dLens[nC] = hypot(pnt.x, pnt.y) / dL;
            } else if (pnt.x < 0 && pnt.y > 0) {
                dLens[nC] = hypot(pnt.x, pnt.y) / dL;
            } else {
                dLens[nC] = hypot(pnt.x, pnt.y) / dL;
            }
            nC++;
        }

        // holds <thetas, lens>
        return dLens;
    }

    protected void assignPolarPoints(ArrayList<Point> pntCs) {
        setDegrees(convertToPolarDegs(pntCs));
        setLengths(convertToPolarLens(pntCs));
    }

    @Override
    public void draw(Graphics g) {
        nXCoords = new int[dDegrees.length];
        nYCoords = new int[dDegrees.length];
        // need this as well
        pntCoords = new Point[dDegrees.length];

        for (int nC = 0; nC < dDegrees.length; nC++) {
            nXCoords[nC] = (int) (getCenter().x + getRadius()
                    * dLengths[nC]
                    * Math.sin(Math.toRadians(getOrientation()) + dDegrees[nC]));
            nYCoords[nC] = (int) (getCenter().y - getRadius()
                    * dLengths[nC]
                    * Math.cos(Math.toRadians(getOrientation()) + dDegrees[nC]));

            // need this line of code to create the points which we will need for debris
            pntCoords[nC] = new Point(nXCoords[nC], nYCoords[nC]);
        }
        g.setColor(getColor());
        g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
    }

    public Point[] getObjectPoints() {
        return pntCoords;
    }

    public void setObjectPoints(Point[] pntPs) {
        pntCoords = pntPs;
    }

    public void setObjectPoint(Point pnt, int nIndex) {
        pntCoords[nIndex] = pnt;
    }

    public int getFadeValue() {
        return nFade;
    }

    public void setFadeValue(int n) {
        nFade = n;
    }

}
