package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by patricklau on 11/27/16.
 */
public class Vacuum extends Sprite implements Scorable {

    public static boolean bGoingUp; // indicates vacuum movement direction (up or down)
    public static int nCollisionCount = 0;
    private boolean bProtected; // for fade in and out
    private boolean bClogged; // indicates whether vacuum is clogged
    private boolean bReproduce = true; // this ensures that will only trigger reproduction once
    private int moveSpeed;
    private final int MAX_SPEED = 40;

    public Vacuum(int moveSpeed, int nSpawnCorner) {
        super();
        setTeam(Team.FOE);

        assignPolarPoints(modelVacuumRight());

        setColor(Color.ORANGE);
        this.moveSpeed = moveSpeed;
        setCenter(pointToSpawn(nSpawnCorner)); // uses method to choose a random corner to spawn at
        setDeltaX(this.moveSpeed);
        setRadius(STANDARD_RADIUS * 5); // this is the size of the vacuum

        // these are vacuum specific
        bClogged = false;
        setProtected(true);
        setFadeValue(0);
    }

    private Point pointToSpawn(int nSpawnCorner) {
        Point point = new Point();
        switch (nSpawnCorner) {
            case 0:
                point = new Point(50, 50); // spawn at top left
                //setOrientation(RIGHT); // with right orientation
                bGoingUp = false;
                break;
            case 1:
                point = new Point(50, getDim().height - 50); // spawn at bottom left
                //setOrientation(RIGHT); // with right orientation
                bGoingUp = true;
                break;
            case 2:
                point = new Point(getDim().width - 50, 50); // spawn at top right
                this.moveSpeed *= -1;
                //setOrientation(LEFT); // with left orientation
                bGoingUp = false;
                break;
            case 3:
                point = new Point(getDim().width - 50, getDim().height - 50); // spawn at bottom right
                this.moveSpeed *= -1;
                //setOrientation(LEFT); // with left orientation
                bGoingUp = true;
                break;
            default:
                break;
        }
        return point;
    }

    // method for getting vacuum to turn

    public void turn() {
        if (getCenter().y < 40) { // when starting at top or reaching the top, then instructs to move down
            bGoingUp = false;
            setCenter(new Point(getCenter().x, getCenter().y + 50));
        }
        if (getCenter().y > getDim().height - 40) { // when starting at or reaching the bottom, then instructs to move up
            setCenter(new Point(getCenter().x, getCenter().y - 50));
            bGoingUp = true;
        }
        if (getCenter().x > getDim().width - 50) { // defines behavior when reaching right wall
            setDeltaX(getDeltaX() * -1);
            //setOrientation((getOrientation() + 180) % 360);
            if (!bGoingUp) { // descend a row if not going up
                setCenter(new Point(getCenter().x - 10, getCenter().y + 50));
            }
            if (bGoingUp) { // ascend a row if going up
                setCenter(new Point(getCenter().x - 10, getCenter().y - 50));
            }
        }
        if (getCenter().x < 50) { // defines behavior when reaching left wall
            setDeltaX(getDeltaX() * -1);
            //setOrientation((getOrientation() + 180) % 360);
            if (!bGoingUp) { // descend a row if not going up
                setCenter(new Point(getCenter().x + 10, getCenter().y + 50));
            }
            if (bGoingUp) { // ascend a row if going up
                setCenter(new Point(getCenter().x + 10, getCenter().y - 50));
            }
            Game.spawnVacuumCountdown(); // spawn additional vacuums if level difficult requires it
        }

        // spawn additional vacuums if level difficult requires it,
        if (bReproduce && !bGoingUp && getCenter().y > 200) { //after providing enough clearance at the top
            Game.spawnVacuumCountdown(); // spawn additional vacuums if level difficulty requires it
            bReproduce = false; // vacuum can no longer spawn additional vacuums
        }
        // spawn additional vacuums if level difficulty requires it,
        if (bReproduce && bGoingUp && getCenter().y < getDim().height - 200) { // after providing enough clearance at the bottom
            Game.spawnVacuumCountdown();
            bReproduce = false; // vacuum can no longer spawn additional vacuums
        }
    }

    public void move() {
        super.move(STICKS, STICKS);
        turn();
        // this line will limit the max speed of the vacuum
        if (getDeltaX() > MAX_SPEED) {
            setDeltaX(MAX_SPEED);
        }
        // if vacuum is clogged, then continuously animate debris
        if (getDeltaX() == 0) {
            Point debrisPoint = new Point(this.getCenter().x - Game.R.nextInt(60), this.getCenter().y + Game.R.nextInt(20));
            CommandCenter.getInstance().getOpsList().enqueue(new DebrisClogged(10, debrisPoint),
                    CollisionOp.Operation.ADD);
        }
        if (getDeltaX() > 0) { // moving right
            assignPolarPoints(modelVacuumRight());
        }
        if (getDeltaX() < 0) { // moving left
            assignPolarPoints(modelVacuumLeft());
        }
    }

    // vacuums are clogged by hairballs

    public void setProtected(boolean bParam) {
        if (bParam) {
            setFadeValue(0);
        }
        bProtected = bParam;
    }

    // getter for the vertical direction, returns true if going up and false otherwise
    public boolean getVerticalDirection() {
        return bGoingUp;
    }

    // setter to reverse the vertical direction
    public void setVerticalDirection() {
        bGoingUp = !bGoingUp; // reverse the boolean
    }

    public void increaseCollisionCount() {
        nCollisionCount++;
    }

    public int getCollisionCount() {
        return nCollisionCount;
    }

    public void setCollisionCount(int nCollisionCount) {
        nCollisionCount = this.nCollisionCount;
    }

    private ArrayList<Point> modelVacuumLeft() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(-154, -58));
        pntCs.add(new Point(-153, -150));
        pntCs.add(new Point(-128, -142));
        pntCs.add(new Point(-123, -109));
        pntCs.add(new Point(-109, -96));
        pntCs.add(new Point(-98, -84));
        pntCs.add(new Point(-90, -68));
        pntCs.add(new Point(-90, -47));
        pntCs.add(new Point(-97, -31));
        pntCs.add(new Point(-102, -26));
        pntCs.add(new Point(-89, -23));
        pntCs.add(new Point(-78, -47));
        pntCs.add(new Point(-67, -56));
        pntCs.add(new Point(-50, -54));
        pntCs.add(new Point(-18, -47));
        pntCs.add(new Point(85, 30));
        pntCs.add(new Point(86, 34));
        pntCs.add(new Point(97, 42));
        pntCs.add(new Point(107, 48));
        pntCs.add(new Point(108, 65));
        pntCs.add(new Point(127, 75));
        pntCs.add(new Point(140, 81));
        pntCs.add(new Point(165, 146));
        pntCs.add(new Point(158, 152));
        pntCs.add(new Point(126, 89));
        pntCs.add(new Point(103, 74));
        pntCs.add(new Point(95, 83));
        pntCs.add(new Point(83, 79));
        pntCs.add(new Point(-97, 2));
        pntCs.add(new Point(-101, -8));
        pntCs.add(new Point(-108, -19));
        pntCs.add(new Point(-127, -12));
        pntCs.add(new Point(-129, -2));
        pntCs.add(new Point(-138, -1));
        pntCs.add(new Point(-137, -17));
        pntCs.add(new Point(-138, -25));
        pntCs.add(new Point(-149, -29));
        pntCs.add(new Point(-158, -37));
        pntCs.add(new Point(-148, -51));

        return pntCs;
    }

    private ArrayList<Point> modelVacuumRight() {
        ArrayList<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(-152, 150));
        pntCs.add(new Point(-152, 58));
        pntCs.add(new Point(-147, 50));
        pntCs.add(new Point(-155, 42));
        pntCs.add(new Point(-144, 28));
        pntCs.add(new Point(-135, 24));
        pntCs.add(new Point(-136, -1));
        pntCs.add(new Point(-125, 1));
        pntCs.add(new Point(-123, 12));
        pntCs.add(new Point(-112, 15));
        pntCs.add(new Point(-102, 14));
        pntCs.add(new Point(-97, 0));
        pntCs.add(new Point(96, -81));
        pntCs.add(new Point(102, -78));
        pntCs.add(new Point(127, -88));
        pntCs.add(new Point(156, -153));
        pntCs.add(new Point(165, -149));
        pntCs.add(new Point(143, -81));
        pntCs.add(new Point(111, -67));
        pntCs.add(new Point(110, -63));
        pntCs.add(new Point(110, -55));
        pntCs.add(new Point(85, -30));
        pntCs.add(new Point(-21, 49));
        pntCs.add(new Point(-72, 59));
        pntCs.add(new Point(-81, 52));
        pntCs.add(new Point(-92, 25));
        pntCs.add(new Point(-104, 26));
        pntCs.add(new Point(-92, 41));
        pntCs.add(new Point(-91, 63));
        pntCs.add(new Point(-119, 105));
        pntCs.add(new Point(-126, 139));

        return pntCs;
    }

    @Override
    public int getScore() {
        return (10 * (int) (Math.abs(getDeltaX()))); // vacuums award points based on its speed
    }
}
