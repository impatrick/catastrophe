package _08final.mvc.view;

import _08final.mvc.controller.Game;
import _08final.mvc.model.CommandCenter;
import _08final.mvc.model.Falcon;
import _08final.mvc.model.Movable;
import _08final.mvc.model.Sprite;

import java.awt.*;
import java.util.ArrayList;


public class GamePanel extends Panel {

    // the following "off" vars are used for the off-screen double-buffered image
    private Dimension dimOff;
    private Image imgOff;
    private Graphics grpOff;
    private GameFrame gmf;
    private Font fnt = new Font("Courier New", Font.BOLD, 12);
    private Font fntBig = new Font("Courier New", Font.BOLD + Font.ITALIC, 36);
    private FontMetrics fmt;
    private int nFontWidth;
    private int nFontHeight;
    private String strDisplay = "";

    public GamePanel(Dimension dim) {
        gmf = new GameFrame();
        gmf.getContentPane().add(this);
        gmf.pack();
        initView();

        gmf.setSize(dim);
        gmf.setTitle("Game Base");
        gmf.setResizable(false);
        gmf.setVisible(true);
        this.setFocusable(true);
    }

    private void drawScore(Graphics g) {
        g.setColor(Color.white);
        g.setFont(fnt);
        if (CommandCenter.getInstance().getScore() != 0) {
            g.drawString("LEVEL " + CommandCenter.getInstance().getLevel() + " |  SCORE :  " + CommandCenter.getInstance().getScore() +
                    " | EXPLODED VACUUMS :  " + Game.getVacuumKills(), nFontWidth, nFontHeight);
        } else {
            g.drawString("LEVEL " + CommandCenter.getInstance().getLevel() + " |  NO SCORE", nFontWidth, nFontHeight);
        }
    }

    public void update(Graphics g) {
        if (grpOff == null || Game.DIM.width != dimOff.width
                || Game.DIM.height != dimOff.height) {
            dimOff = Game.DIM;
            imgOff = createImage(Game.DIM.width, Game.DIM.height);
            grpOff = imgOff.getGraphics();
        }
        // fill in background with black
        grpOff.setColor(Color.black);
        grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

        drawScore(grpOff);

        if (!CommandCenter.getInstance().isPlaying()) {
            displayTextOnScreen();
        } else if (CommandCenter.getInstance().isPaused()) {
            strDisplay = "Game Paused";
            grpOff.drawString(strDisplay,
                    (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
        }

        // playing and not paused
        else {
            // draw them in decreasing level of importance
            // friends will be on top layer and debris on the bottom
            iterateMovables(grpOff,
                    (ArrayList<Movable>) CommandCenter.getInstance().getMovFriends(),
                    (ArrayList<Movable>) CommandCenter.getInstance().getMovFoes(),
                    (ArrayList<Movable>) CommandCenter.getInstance().getMovFloaters(),
                    (ArrayList<Movable>) CommandCenter.getInstance().getMovDebris());

            drawNumberLivesLeft(grpOff);
            if (CommandCenter.getInstance().isGameOver()) {
                CommandCenter.getInstance().setPlaying(false);
                // bPlaying = false;
            }
        }
        // draw the double-Buffered Image to the graphics context of the panel
        g.drawImage(imgOff, 0, 0, this);
    }


    // for each movable array, process it.
    private void iterateMovables(Graphics g, ArrayList<Movable>... movMovz) {

        for (ArrayList<Movable> movMovs : movMovz) {
            for (Movable mov : movMovs) {
                mov.move();
                mov.draw(g);
            }
        }

    }

    // draw the number of falcons left on the bottom-right of the screen.
    private void drawNumberLivesLeft(Graphics g) {
        Falcon fal = new Falcon(Color.white);
        double[] dLens = fal.getLengths();
        int nLen = fal.getDegrees().length;
        Point[] pntMs = new Point[nLen];
        int[] nXs = new int[nLen];
        int[] nYs = new int[nLen];

        // convert to cartesian points
        for (int nC = 0; nC < nLen; nC++) {
            pntMs[nC] = new Point((int) (10 * dLens[nC] * Math.sin(Math
                    .toRadians(90) + fal.getDegrees()[nC])),
                    (int) (10 * dLens[nC] * Math.cos(Math.toRadians(90)
                            + fal.getDegrees()[nC])));
        }

        // set the color to white
        g.setColor(Color.white);
        fal.setOrientation(Sprite.UP);
        // modified so that the current life is included in display
        // for each falcon left (including the one that is playing)
        for (int nD = 1; nD < CommandCenter.getInstance().getNumFalcons() + 1; nD++) {
            // create x and y values for the objects to the bottom right using cartesian points again
            for (int nC = 0; nC < fal.getDegrees().length; nC++) {
                nXs[nC] = pntMs[nC].x + Game.DIM.width - (20 * nD);
                nYs[nC] = pntMs[nC].y + Game.DIM.height - 40;
            }
            g.drawPolygon(nXs, nYs, nLen);
        }
    }

    private void initView() {
        Graphics g = getGraphics(); // get the graphics context for the panel
        g.setFont(fnt); // take care of some simple font stuff
        fmt = g.getFontMetrics();
        nFontWidth = fmt.getMaxAdvance();
        nFontHeight = fmt.getHeight();
        g.setFont(fntBig); // set font info
    }

    // This method draws some text to the middle of the screen before/after a game
    private void displayTextOnScreen() {

        strDisplay = "   ____       _______         ____ _______ _____   ____  _____  _    _ ______ ";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 40);

        strDisplay = " / ____|   /\\|__   __|/\\    / ____|__   __|  __ \\ / __ \\|  __ \\| |  | |  ____|";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 55);

        strDisplay = "| |       /  \\  | |  /  \\  | (___    | |  | |__) | |  | | |__) | |__| | |__   ";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 70);

        strDisplay = "| |      / /\\ \\ | | / /\\ \\  \\___ \\   | |  |  _  /| |  | |  ___/|  __  |  __|  ";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 85);

        strDisplay = "| |____ / ____ \\| |/ ____ \\ ____) |  | |  | | \\ \\| |__| | |    | |  | | |____ ";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 100);

        strDisplay = " \\_____/_/    \\_\\_/_/    \\_\\_____/   |_|  |_|  \\_\\\\____/|_|    |_|  |_|______|";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 115);

        strDisplay = "THE GAME OF CATASTROPHE! ^_^";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 200);

        strDisplay = "use the arrow keys to turn and crawl -.-";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 240);

        strDisplay = "use the space bar to cough up hairballs >.<";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 280);

        strDisplay = "'S' to Start";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 320);

        strDisplay = "'P' to Pause";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 360);

        strDisplay = "'Q' to Quit";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 400);
        strDisplay = "run away from vacuums!";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 440);

        strDisplay = "left index finger on 'F' to throw up and explode a vacuum! (must eat a red mouse first)";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + nFontHeight + 480);

    }

    public GameFrame getFrm() {
        return this.gmf;
    }

    public void setFrm(GameFrame frm) {
        this.gmf = frm;
    }
}