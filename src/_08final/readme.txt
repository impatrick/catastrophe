
    ____       _______         ____ _______ _____   ____  _____  _    _ ______
  / ____|   /\|__   __|/\    / ____|__   __|  __ \ / __ \|  __ \| |  | |  ____|
 | |       /  \  | |  /  \  | (___    | |  | |__) | |  | | |__) | |__| | |__
 | |      / /\ \ | | / /\ \  \___ \   | |  |  _  /| |  | |  ___/|  __  |  __|
 | |____ / ____ \| |/ ____ \ ____) |  | |  | | \ \| |__| | |    | |  | | |____
  \_____/_/    \_\_/_/    \_\_____/   |_|  |_|  \_\\____/|_|    |_|  |_|______|

 =============
 GAMEPLAY
 =============

 Catastrophe is a single-player shoot 'em up style arcade game.
 You assume the role of a destructive cat named Cat.
 Vacuums spawn in pseudo random locations around the room.
 Vacuums follow a path bouncing from walls left to right and up and down.
 Defend yourself by coughing up hairballs (hit Space). Hairballs clog vacuums, but do not kill them.
 Barfing on a vacuum (hit F) will make it explode and you get points.
 You get the barf ability by consuming special floaters.

 =============
 CODE FEATURES
 =============
 *Added foe on foe collision mechanics (Game:298)
 *Added scoring scorable (Game:447)
 *Added extra lives (nine obviously) with cat graphic
 *Litterboxes spawn on the map, and provide Cat a safe place of refuge (Game:182)
 *But a vacuum will bump the litterbox in the direction of travel, so the cat cannot simply hide in a litterbox (Game:398)
 *Vacuums can bump into each other, but several bumps will cause them to explode (Game:320)
 *Vacuum needed a lot of coding to move and turn logically (Vacuum:72)
 *Blue floaters makes the cat invincible and spawn awesome trailing graphics, for a short timed period (Game:251)
 *I composed and sequenced the music myself using 8bit sounds! (Game:80)

 =============
 DEBRIS
 =============
 *Added debris upon vacuum explosion (Game:443)
 *Added debris from cat kill (Game:179)
 *Added debris when vacuum is stopped (Vacuum:125)
 *Added debris when cat is invincible from a blue floater (Game:724)